#!/bin/bash

if [ -z "$1" ]; then
    echo "Region must be provided"
    exit 1
else
    regionString="--region $1"
    region="$1"
fi

if [ -z "$2" ]; then
    echo "Profile option not provided, using default"
    profileString=""
else
    profileString="--profile $2"
fi

accountNumber=$(aws sts get-caller-identity --output text --query 'Account' $profileString)

output=$(aws iam create-role --role-name LambdaAPI --assume-role-policy-document file://scripts/role-policy.json $profileString --path "/service/role/" | jq ".Role.Arn")
serviceRole=${output//[\"]/ }
serviceRole=$(echo $serviceRole | xargs)
echo "Created new service role: $serviceRole"

aws iam put-role-policy --role-name LambdaAPI --policy-name LambdaExecute --policy-document file://scripts/additional-role-policy.json $profileString

echo "About to sleep.... I don't know why this is needed, but it seems to make it work"
sleep 10

cd node
zip lambda.zip handler.js
command="aws lambda create-function --function-name lambda-api --runtime nodejs8.10 --role $serviceRole --handler handler.hello --zip-file 'fileb://lambda.zip' $profileString $regionString"
echo $command
output=$(eval $command)
rm lambda.zip

output=$(aws apigateway create-rest-api --name lambda-api $profileString $regionString)
gatewayId=$(echo $output | jq ".id")
gatewayId=${gatewayId//[\"]/ }
gatewayId=$(echo $gatewayId | xargs)
echo "Gateway created: $gatewayId"

output=$(aws apigateway get-resources --rest-api-id $gatewayId $profileString $regionString)
parentApiId=$(echo $output | jq ".items[0].id")
parentApiId=${parentApiId//[\"]/ }
parentApiId=$(echo $parentApiId | xargs)
echo "Parent Resource: $parentApiId"

aws apigateway put-method --rest-api-id $gatewayId --resource-id $parentApiId --http-method GET --authorization-type NONE $profileString $regionString

aws apigateway put-integration --rest-api-id $gatewayId --resource-id $parentApiId \
--http-method GET --type AWS --integration-http-method POST \
--uri arn:aws:apigateway:$region:lambda:path/2015-03-31/functions/arn:aws:lambda:$region:$accountNumber:function:lambda-api/invocations \
$profileString $regionString

aws apigateway put-method-response --rest-api-id $gatewayId \
--resource-id $parentApiId --http-method GET \
--status-code 200 --response-models application/json=Empty $profileString $regionString

aws apigateway put-integration-response --rest-api-id $gatewayId \
--resource-id $parentApiId --http-method GET \
--status-code 200 --response-templates application/json="" $profileString $regionString

aws apigateway create-deployment --rest-api-id $gatewayId --stage-name prod $profileString $regionString

aws lambda add-permission --function-name lambda-api \
--statement-id apigateway-test-2 --action lambda:InvokeFunction \
--principal apigateway.amazonaws.com \
--source-arn "arn:aws:execute-api:$region:$accountNumber:$gatewayId/*/GET/" \
$profileString $regionString

aws lambda add-permission --function-name lambda-api \
--statement-id apigateway-prod-2 --action lambda:InvokeFunction \
--principal apigateway.amazonaws.com \
--source-arn "arn:aws:execute-api:$region:$accountNumber:$gatewayId/prod/GET/" \
$profileString $regionString
