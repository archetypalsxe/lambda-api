const handler = require('../handler');

test('correct greeting is generated', () => {

    return handler.hello().then(data => {
        var timeStamp = Math.floor(new Date().getTime() / 1000);
        expect(data.statusCode).toBe(200);
        var body = JSON.parse(data.body);
        expect(body.message).toBe("Automation for the People");
        /*
         * A second may pass while test is running before generating timestamp...
         * Leaving 5 seconds cushion just to be sure
         */
        expect(body.timestamp).toBeGreaterThanOrEqual(timeStamp - 5);
        // Should never be less than a timestamp generated after call
        expect(body.timestamp).toBeLessThanOrEqual(timeStamp);
    });

});
