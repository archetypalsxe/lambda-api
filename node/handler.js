'use strict';

module.exports.hello = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Automation for the People',
      // Converting time in milliseconds to seconds
      timestamp: Math.floor(new Date().getTime() / 1000),
    }),
  };
};
