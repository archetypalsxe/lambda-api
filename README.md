# Lambda API

Runs a lambda function with an API gateway in front of it for directing traffic

## Local Development Requirements
* serverless NPM package installed
  * `sudo npm install -g serverless`
* lambda Docker image:
  * `docker pull lambci/lambda`

## Testing
* Install yarn
* In the `node` folder:
  * `yarn install`
  * `yarn run jest`

## Local Usage
* In the `node` folder:
  * `serverless offline start`
* Navigate (or curl) `localhost:3000`

## Initial Deployment
* Requires jq for parsing bash responses
* Run `scripts/deploy.sh REGION PROFILE` from root directory
  * REGION example: `us-east-2`
  * PROFILE is optional and will be whatever your AWS credentials are setup as

## Jenkins Setup
### Requirements
* yarn
* npm
* nodejs
* aws cli
* Jenkins Plugins:
  * AWS Lambda
* Setup git repo credentials
* Setup AWS credentials
### Setting Up Project
* Create new Jenkins pipeline project
* Enable `GitHub hook trigger for GITScm polling`
* Pipeline
  * Definition: `Pipeline script from SCM`
  * Enter repository details
* In repo, modify:
  * Jenkinsfile
    * credentialsId needs to be updated
    * awsRegion if not using us-east-1
